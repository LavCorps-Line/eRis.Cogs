# PREAMBLE
"eRis" runs on the social online and mobile chat platform hosted by Discord, Inc.
[Click here for more information about the privacy policy of Discord, Inc.](https://discord.com/privacy)
This privacy policy focuses ONLY on the eRis Cogs, and NOT the eRis Core, as defined below.

# TERMINOLOGY
Discord refers to the social online and mobile chat platform hosted by Discord, Inc.

A "Guild" or "Server" refers to the "Guild" objects stored on Discord.

eRis is developed and hosted by an independent group of people, hereinafter referred to as the LavCorps-Line or LavCorps.

The eRis Cogs provide the user-facing commands for eRis.

A "Display Name" is the name a user presents to other users of Discord.

# INFORMATION COLLECTED
## "STATE CHANGES"
Due to constraints in the API used for eRis, huge amounts of data may be "collected" but stay untouched by the eRis Cogs. This information is stored in volatile memory for as long as the cache doesn't expire. A non-exhaustive list of information collected includes:
* The name, discriminator, snowflake, avatar, status, status message, and current in-game status of the user that caused the state change.
* The name, snowflake, position, settings, and category of the channel that caused the state change.
* The name, snowflake, avatar, members, roles, and settings of the server that caused the state change.
* The content, attachments, embedded information, snowflake, edit time, send time, and channel of the message that caused the state change.

## "PREFIX" INTERACTIONS
When you interact with eRis through the eRis Cogs "Prefix" system, information is collected, listed exhaustively below.
* Display Name & Avatar
    * We use Display Names and Avatars to provide immersive and realistic responses to user prompts.
* Full message contents including Text, Attachments (Images or otherwise), and Other Embedded Information, such as time sent, edited, and previews for links and videos.
    * We use this information to enable the eRis Cogs to process input and respond accordingly. Information about the user is not stored following a "Prefix" interaction. However, user input may be stored in the form of configuration data when the "Prefix" interaction refers to a configuration function.
* Other Data
    * Other data may be collected as part of a "Prefix" interaction. However, if it lacks an entry above, it is likely data not known to be collected.

Save for any exceptions noted above, user data is never permanently stored, and is discarded after function completion.

## "SERVERLOG" LOGGING
When you're on a server with eRis, the staff of that server may have configured the Eris Cog "Serverlog". In a "Serverlog" configuration with all features enabled, information is collected, listed exhaustively below.
* Display Name, Avatar, Discriminator, Snowflake ID
    * We use Display Names, Avatars, Discriminators, and Snowflake IDs when logging Message Edit, Message Deletion, Member Update, Member Ban, Member Unban, Member Join, and Member Leave events as defined in the "Serverlog" documentation.
* Message Text, as well as Attachments (Images or otherwise), and Other Embedded Information
    * We use Message Text, Attachments (Images or otherwise), and Other Embedded Information when logging Message Edit and Message Deletion events as defined in the "Serverlog" documentation.

Save for any exceptions noted above, user data is never permanently stored, and is discarded after function completion. However, the processed user data is available in a designated "Serverlog Channel". Please direct questions of data handling to relevant Server staff.

# WHERE INFORMATION IS PROCESSED
eRis is hosted in the United States. No matter where you are located, you consent to the processing and transferring of your information in and to the U.S. and other countries.

# DISCLOSURE OF DATA
eRis does Not, under Any circumstances, intentionally share information with a 3rd party, without consent, regardless of any anticipation of consent.
