#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/welcome
## a component of eRis.Cogs
* Automatically welcomes users to a server
"""

# Standard Library Imports #
import io

# Third-Party Imports #
import discord
import discord.ext.commands as commands

# First-Party Imports #
import eris.bot
import eris.cog
import eris.exceptions
from eris.cogs.libConfig.lockman import lockManager
from eris.cogs.libCore import libCore
from eris.logger import logger


class welcome(eris.cog.Cog):
    """Automated introductions for users to the server!"""

    def __init__(self, **kwargs):
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.core: libCore = kwargs["deps"]["libCore"]
        self.log = logger("eRis.cogs.welcome")

    @commands.hybrid_group()
    @commands.guild_only()
    @commands.has_guild_permissions(manage_messages=True, manage_guild=True)
    async def welcome(self, ctx):
        """Automatic Welcoming Configuration"""
        pass

    @welcome.command()
    @commands.bot_has_permissions(embed_links=True)
    async def status(self, ctx: commands.Context):
        """Check the status of the automatic welcoming system."""
        async with lockManager(snf=ctx.guild, bot=self.bot, cog=self) as data:
            if "channel" not in data:
                data["channel"] = None
            if "message" not in data:
                data["message"] = None
            if "compile" not in data:
                data["compile"] = False
            welcomeEmbed = discord.Embed(
                title="Automatic Welcome Configuration",
                color=discord.Color.random(),
            )
            channel = self.bot.get_channel(data["channel"]) if data["channel"] else None
            welcomeEmbed.add_field(
                name="Destination Channel",
                value=channel if channel else "None, **automatic welcomes are disabled**.",
            )
            welcomeEmbed.add_field(
                name="Welcome Message",
                value=(
                    data["message"]
                    if data["message"]
                    else "None, **automatic welcomes are disabled**."
                ),
            )
            welcomeEmbed.add_field(
                name="Message Compile Check",
                value=(
                    "Message compiles successfully."
                    if data["compile"]
                    else "Message fails to compile or has not yet been compiled, **automatic welcomes are disabled**."
                ),
            )

            await ctx.send(embed=welcomeEmbed)

    @welcome.command()
    async def channel(self, ctx, channel: discord.TextChannel | None = None):
        """Set the welcome message's destination channel. If unspecified, disables automatic welcomes."""
        async with lockManager(snf=ctx.guild, bot=self.bot, cog=self) as data:
            if channel is None:  # TODO: rewrite error catch logic here
                try:
                    data["channel"] = None
                except Exception as e:
                    await ctx.send(
                        "Failed to clear the message destination channel.\n" + str(repr(e))
                    )
                    raise e
                else:
                    await ctx.send("Successfully cleared the message destination channel.")
            else:
                try:
                    data["channel"] = channel.id
                except Exception as e:
                    await ctx.send(
                        "Failed to set the message destination channel.\n" + str(repr(e))
                    )
                    raise e
                else:
                    await ctx.send("Successfully set the message destination channel.")

    @welcome.command()
    async def message(self, ctx, *, message: str | None = None):
        """Set the welcome message contents. If unspecified, deletes welcome message.

        Extends API attributes from [discord.User](https://discordpy.readthedocs.io/en/latest/api.html#user) as `{user}`, [discord.Member](https://discordpy.readthedocs.io/en/latest/api.html#member) as `{user}`, and [discord.Guild](https://discordpy.readthedocs.io/en/latest/api.html#guild) as `{guild}` and `{server}`.

        Example message: ```\nWelcome {user.mention} to {server.name}!\n```"""
        async with lockManager(snf=ctx.guild, bot=self.bot, cog=self) as data:
            if message is None:  # TODO: rewrite error catch logic here
                try:
                    if "message" in data:
                        wlcMsg = data["message"]
                    else:
                        wlcMsg = None
                    data["message"] = None
                    data["compile"] = False
                except Exception as e:
                    await ctx.send(
                        "Failed to clear the welcome message.\n" + str(repr(e)), ephemeral=True
                    )
                    raise e
                else:
                    if wlcMsg:
                        await ctx.send(
                            "Successfully cleared the welcome message.\n"
                            "In case you didn't mean to do that, the old welcome message is attached below.",
                            file=discord.File(
                                io.BytesIO(wlcMsg.encode()),
                                filename="old_message.txt",
                            ),
                            ephemeral=True,
                        )
                    else:
                        await ctx.send("No old welcome message was found.", ephemeral=True)
            else:
                try:
                    if "message" in data:
                        wlcMsg = data["message"]
                    else:
                        wlcMsg = None
                    data["message"] = message
                    data["compile"] = False
                except Exception as e:
                    await ctx.send(
                        "Failed to set the welcome message.\n" + str(repr(e)), ephemeral=True
                    )
                    raise e
                else:
                    if wlcMsg:
                        await ctx.send(
                            "Successfully set the welcome message.\n"
                            "In case you didn't mean to do that, the old welcome message is attached below.",
                            file=discord.File(
                                io.BytesIO(wlcMsg.encode()), filename="old_message.txt"
                            ),
                            ephemeral=True,
                        )
                    else:
                        await ctx.send("Successfully set the welcome message.", ephemeral=True)

    @welcome.command()
    async def test(self, ctx):
        """Test compilation of the welcome message.
        You must run this at least once after setting the welcome message!"""
        async with lockManager(snf=ctx.guild, bot=self.bot, cog=self) as data:
            if not self.bot.get_channel(data["channel"]):
                await ctx.send(
                    "Server has no message destination channel set, therefore automatic welcomes are disabled.",
                    ephemeral=True,
                )
            if not data["message"]:
                await ctx.send(
                    "Server has no welcome message set, therefore automatic welcomes are disabled.",
                    ephemeral=True,
                )

            if not self.bot.get_channel(data["channel"]) or not data["message"]:
                return

            try:
                await ctx.send(
                    str(data["message"]).format(
                        user=ctx.author, server=ctx.guild, guild=ctx.guild
                    ),
                    ephemeral=True,
                )
            except Exception as e:
                await ctx.send(
                    "Message failed to compile.\n{}".format(str(repr(e))), ephemeral=True
                )
                data["compile"] = False
            else:
                await ctx.send("Message successfully compiled!", ephemeral=True)
                data["compile"] = True

    @eris.cog.Cog.listener(name="on_member_join")
    async def _member_join(self, member: discord.Member):
        async with lockManager(snf=member.guild, bot=self.bot, cog=self) as data:
            if "channel" not in data or "message" not in data or "compile" not in data:
                return
            elif (
                not self.bot.get_channel(data["channel"])
                or not data["message"]
                or not data["compile"]
            ):
                return

            await self.bot.get_channel(data["channel"]).send(
                str(data["message"]).format(user=member, server=member.guild, guild=member.guild)
            )
