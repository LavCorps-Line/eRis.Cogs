#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/utils
## a component of eRis.Cogs
* Provides utility commands for users
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .utils import utils


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=utils(**kwargs))
