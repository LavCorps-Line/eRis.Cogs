#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/utils
## a component of eRis.Cogs
* Provides utility commands for users
"""

# Standard Library Imports #
import datetime
import io
import time

# Third-Party Imports #
import discord
import discord.ext.commands as commands

# First-Party Imports #
import eris.bot
import eris.cog


def embedifyMessage(message):
    """Return a message as an embed."""
    embed = discord.Embed(description=message.content)
    embed.set_author(name=message.author.name, icon_url=message.author.avatar.url)
    embed.set_footer(text="{}".format(str(message.id)))
    embed.timestamp = message.created_at
    return embed


class utils(eris.cog.Cog):
    """Some simple utilities for stuff like colors, channel editting, and message re-sending!"""

    def __init__(self, **kwargs):
        self.bot: eris.bot.Bot = kwargs["bot"]

    @commands.hybrid_command()
    @commands.bot_has_permissions(attach_files=True)
    async def spoiler(self, ctx):
        """Resends an attached image, spoilering it"""
        if not ctx.message.attachments:
            await ctx.send("I can't spoiler nothing!", ephemeral=True)
            return

        ramfileWraps = []

        for x in range(len(ctx.message.attachments)):
            ramfile = io.BytesIO()
            try:
                ramfile.write(await ctx.message.attachments[x].read())
            except Exception as a:
                try:
                    ramfile.write(await ctx.message.attachments[x].read(use_cached=True))
                except Exception as b:
                    await ctx.send(
                        "Failed to download file.\n```py\n{}\n{}\n```".format(
                            str(repr(a)), str(repr(b))
                        )
                    )
                    return
            ramfile.seek(0)
            ramfileWrapped = discord.File(
                ramfile, filename=ctx.message.attachments[x].filename, spoiler=True
            )
            ramfileWraps.append(ramfileWrapped)

        try:
            spoilered = await ctx.send(embed=embedifyMessage(ctx.message), files=ramfileWraps)
        except Exception as e:
            await ctx.send("Failed to send file.\n{}".format(str(repr(e))), ephemeral=True)
        else:
            try:
                await ctx.message.delete()
            except Exception as e:
                await ctx.send(
                    "Failed to delete trigger message.\n{}".format(repr(e)), ephemeral=True
                )
                await spoilered.delete()

    @staticmethod
    def clamp(x: int) -> int:
        """Clamp between 0 and 255."""
        return max(0, min(x, 255))

    @staticmethod
    def checkHex(hx):
        """Hexadecimal check function"""
        if len(hx) != 6 and len(hx) != 7:
            return False
        hx = hx.replace("#", "0x")
        try:
            dec = int(hx, 16)
        except ValueError:
            return False
        return dec

    @staticmethod
    def colorEmbed(dec, desc):
        """Return an embed with given title and color"""
        return discord.Embed(title=desc, color=dec)

    @commands.hybrid_command(name="hex")
    @commands.bot_has_permissions(embed_links=True)
    async def hexColor(self, ctx, hx: str):
        """Test hexadecimal colors!"""
        dec = self.checkHex(hx)
        if not dec:
            await ctx.send(
                "The 'hx' argument to this command must be a 6 character hexadecimal string or a 7 character "
                "hexadecimal string prepended with #! "
            )
            return
        await ctx.send(
            embed=self.colorEmbed(
                dec,
                "This is a test message demonstrating hexadecimal color #"
                + hx.replace("#", "").upper()
                + "!",
            ),
            ephemeral=True,
        )

    @commands.hybrid_command()
    async def stopwatch(self, ctx):
        """Start or stop the stopwatch."""
        author = ctx.author
        if author.id not in self.stopwatches:
            self.stopwatches[author.id] = int(time.perf_counter())
            await ctx.send(author.mention + " Stopwatch started!")
        else:
            tmp = abs(self.stopwatches[author.id] - int(time.perf_counter()))
            tmp = str(datetime.timedelta(seconds=tmp))
            await ctx.send(
                author.mention + " Stopwatch stopped! Time: **{seconds}**".format(seconds=tmp)
            )
            self.stopwatches.pop(author.id, None)

    @commands.hybrid_command()
    @commands.bot_has_permissions(embed_links=True)
    async def rgb(self, ctx, r: int, g: int, b: int):
        """Test RGB colors!"""
        r = self.clamp(r)
        g = self.clamp(g)
        b = self.clamp(b)
        await ctx.send(
            embed=self.colorEmbed(
                self.checkHex("%02x%02x%02x" % (r, g, b)),
                "This is a test message demonstrating RGB color " + str(tuple([r, g, b])) + "!",
            ),
            ephemeral=True,
        )

    @commands.hybrid_command(name="rgb2hex")
    @commands.bot_has_permissions(embed_links=True)
    async def RgbToHex(self, ctx, r: int, g: int, b: int):
        """Convert RGB to Hexadecimal!"""
        r = self.clamp(r)
        g = self.clamp(g)
        b = self.clamp(b)
        await ctx.send(
            embed=self.colorEmbed(
                self.checkHex("%02x%02x%02x" % (r, g, b)),
                "This is a test message demonstrating hexadecimal color "
                + "#%02x%02x%02x".upper() % (r, g, b)
                + "!",
            ),
            ephemeral=True,
        )

    @commands.hybrid_command(name="hex2rgb")
    @commands.bot_has_permissions(embed_links=True)
    async def HexToRgb(self, ctx, hx: str):
        """Convert Hexadecimal to RGB!"""
        dec = self.checkHex(hx)
        if not dec:
            await ctx.send(
                "The 'hx' argument to this command must be a 6 character hexadecimal string or a 7 character "
                "hexadecimal string prepended with #! "
            )
            return
        await ctx.send(
            embed=self.colorEmbed(
                dec,
                desc="This is a test message demonstrating RGB color "
                + str(tuple(int(hex[i : i + 2], 16) for i in (0, 2, 4)))
                + "!",
            ),
            ephemeral=True,
        )

    @commands.hybrid_command()
    @commands.bot_has_permissions(embed_links=True)
    async def rolecolor(self, ctx, role: discord.Role):
        """Test role colour!"""
        dec = role.color.value
        if not dec:
            await ctx.send(
                "That role doesn't have a color value\nDid you specify a role that has a color?"
            )
            return
        await ctx.send(
            embed=self.colorEmbed(
                dec,
                desc="This is a test message demonstrating hexadecimal color "
                + hex(dec).replace("0x", "#").upper()
                + "!",
            ),
            ephemeral=True,
        )
