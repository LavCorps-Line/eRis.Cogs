#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/serverlog
## a component of eRis.Cogs
* Allows server staff to define specific logging settings for server activities
"""
# Standard Library Imports #
import asyncio
import base64
import binascii
import bz2
import datetime
import io
import typing
from enum import Enum, auto

# Third-Party Imports #
import discord
import discord.ext.commands as commands
import discord.ui
import discord.utils
import humanize

# First-Party Imports #
import eris.bot
import eris.cog
import eris.exceptions
from eris.cogs.libConfig import libConfig
from eris.cogs.libConfig.lockman import lockManager
from eris.cogs.libCore import libCore
from eris.logger import logger


class case_type:
    """caseType Base Class"""

    def __init__(
        self, name: str, emoji: str, color: discord.Colour | str, involves: type, verb: str
    ):
        # caseType Name
        self.name = name
        # caseType emoji
        self.emoji = emoji
        # caseType emoji codepoint
        self.emojicpoint = hex(ord(list(emoji)[0]))[2:]
        # caseType twemoji display url
        self.emojiurl = "https://twemoji.maxcdn.com/v/latest/72x72/{}.png".format(self.emojicpoint)
        # caseType string representation
        self.string = name.title()
        # caseType involves
        self.involves = involves
        # past tense verb of operation, i.e. "edited"
        self.verb = verb

        if isinstance(color, str) and color.lower() == "random":
            self.color = discord.Color.random()  # caseType color (for embeds)
        else:
            # caseType color (for embeds)
            self.color = color


class case_action(Enum):
    """Enumerated case_actions"""

    MESSAGE_DELETED = auto()
    MESSAGE_EDITED = auto()
    CHANNEL_DELETED = auto()
    CHANNEL_CREATED = auto()
    CHANNEL_EDITED = auto()
    MEMBER_JOINED = auto()
    MEMBER_LEFT = auto()
    MEMBER_UPDATED = auto()
    USER_UPDATED = auto()
    ROLE_CREATED = auto()
    ROLE_DELETED = auto()
    USER_BANNED = auto()
    USER_UNBANNED = auto()
    GUILD_EDITED = auto()
    ROLE_EDITED = auto()
    EMOJI_EDITED = auto()
    INVITE_CREATED = auto()
    INVITE_DELETED = auto()


class case_data:
    """Case Base Class"""

    def __init__(
        self,
        in_case_type: case_type,
        timestamp: datetime.datetime,
        action: case_action,
        guild: discord.Guild,
        data: dict,
        archive: list,
        log_channels: list,
        cog: eris.cog.Cog,
    ):
        self.type = in_case_type
        self.timestamp = timestamp
        self.action = action
        self.guild = guild
        self.data = data
        self.archive = archive
        self.log_channels = log_channels
        self.cog: serverlog = cog

    async def log(self):
        """

        Returns:

        """

        if not self.log_channels:
            return

        try:
            case_embed = await self.make_embed()

            if not case_embed:
                return

            for channel in self.log_channels:
                await channel.send(embed=case_embed)
        except Exception as error:
            self.cog.log.error(
                "Whoops! Exception `{}` occured while processing a `{}` case!".format(
                    type(error).__name__, self.type.name.lower()
                ),
                exc_info=error,
            )
            exceptionEmbed = discord.Embed(
                title="Whoops!",
                description="Exception `{}` occured while processing a `{}` case!".format(
                    type(error).__name__, self.type.name.lower()
                ),
                color=discord.Color.random(),
            ).set_thumbnail(
                url="https://twemoji.maxcdn.com/v/latest/72x72/{}.png".format(
                    hex(ord(list("⚠️")[0]))[2:]
                )
            )
            for channel in self.log_channels:
                try:
                    await channel.send(embed=exceptionEmbed)
                except discord.Forbidden:
                    pass  # TODO: consider sending a message to server owner or staff automatically about this

    async def make_embed(self) -> discord.Embed:
        """

        Returns:

        """
        case_embed = discord.Embed(
            title=self.type.string + " - " + str(self.timestamp)[:-7] + "UTC",
            color=self.type.color,
        )
        case_embed.set_thumbnail(url=self.type.emojiurl)
        case_embed.timestamp = self.timestamp

        if self.type.involves == discord.Message:
            return await self.make_embed_message(case_embed=case_embed)
        elif self.type.involves == discord.User:
            return await self.make_embed_user(case_embed=case_embed)
        elif self.type.involves == discord.abc.GuildChannel:
            return await self.make_embed_channel(case_embed=case_embed)
        elif self.type.involves == discord.Guild:
            return await self.make_embed_guild(case_embed=case_embed)
        elif self.type.involves == discord.Invite:
            return await self.make_embed_invite(case_embed=case_embed)
        else:
            raise NotImplementedError(
                "Asked to make a serverlog message that wasn't for a message, user, channel, or guild!"
            )

    async def make_embed_guild(self, case_embed: discord.Embed) -> discord.Embed:
        """

        Args:
            case_embed:

        Returns:

        """
        if self.action == case_action.GUILD_EDITED:
            before: discord.Guild = self.data["payload"][0]
            after: discord.Guild = self.data["payload"][1]
        elif self.action == case_action.EMOJI_EDITED:
            before = self.guild
            after = self.guild
        elif self.action == case_action.ROLE_EDITED:
            before: discord.Guild = self.data["payload"][0].guild
            after: discord.Guild = self.data["payload"][1].guild
        elif self.action == case_action.ROLE_CREATED or self.action == case_action.ROLE_DELETED:
            before = self.guild
            after = self.guild
        else:
            raise NotImplementedError(
                (
                    "Asked to process a guild case that wasn't for a guild edit, "
                    "emoji edit, or role edit/creation/deletion!"
                )
            )

        case_embed.set_footer(text="Guild ID: {}".format(str(after.id)))
        case_embed.add_field(
            name="Log Info",
            value="`{}` has updated.".format(
                discord.utils.escape_markdown(text=discord.utils.escape_mentions(text=after.name))
            ),
            inline=False,
        )

        if self.action == case_action.GUILD_EDITED:
            if before.name != after.name:
                case_embed.add_field(
                    name="Server Name Changed",
                    value="`{}` ➡️ `{}`".format(
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=before.name)
                        ),
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=after.name)
                        ),
                    ),
                )
            if before.afk_timeout != after.afk_timeout or before.afk_channel != after.afk_channel:
                if (
                    before.afk_timeout != after.afk_timeout
                    and before.afk_channel == after.afk_channel
                ):
                    val = "Timeout: `{}` ➡️ `{}`\n".format(
                        str(before.afk_timeout), str(after.afk_timeout)
                    )
                elif (
                    before.afk_timeout == after.afk_timeout
                    and before.afk_channel != after.afk_channel
                ):
                    val = "Timeout Channel: `{}` ➡️ `{}`".format(
                        str(before.afk_channel), str(after.afk_channel)
                    )
                else:
                    val = "Timeout: `{}` ➡️ `{}`\nTimeout Channel: `{}` ➡️ `{}`".format(
                        str(before.afk_timeout),
                        str(after.afk_timeout),
                        str(before.afk_channel),
                        str(after.afk_channel),
                    )
                case_embed.add_field(name="Server VC Timeout Changed", value=val)
            if before.icon != after.icon:
                case_embed.add_field(
                    name="Server Icon Changed",
                    value="Old: {}\nNew: {}".format(
                        str(before.icon.url if bool(before.icon.url) else None),
                        str(after.icon.url if bool(after.icon.url) else None),
                    ),
                )
            if before.banner != after.banner:
                case_embed.add_field(
                    name="Server Banner Changed",
                    value="Old: {}\nNew: {}".format(str(before.banner), str(after.banner)),
                )
            if before.description != after.description:
                case_embed.add_field(
                    name="Server Description Changed",
                    value="Old: `{}`\nNew: `{}`".format(
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=before.description)
                        ),
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=after.description)
                        ),
                    ),
                )
            if before.mfa_level != after.mfa_level:
                case_embed.add_field(
                    name="Server 2FA Requirements Changed",
                    value="`{}` ➡️ `{}`".format(
                        str("ON" if before.mfa_level else "OFF"),
                        str("ON" if after.mfa_level else "OFF"),
                    ),
                )
            if before.verification_level != after.verification_level:
                case_embed.add_field(
                    name="Server Verification Level Changed",
                    value="`{}` ➡️ `{}`".format(
                        str(before.verification_level).title(),
                        str(after.verification_level).title(),
                    ),
                )
            if (
                before.premium_tier != after.premium_tier
                or before.premium_subscription_count != after.premium_subscription_count
            ):
                case_embed.add_field(
                    name="Server Nitro Boost Status Changed",
                    value="Tier: `{}` ➡️ `{}`\nBoosts: `{}` ➡️ `{}`".format(
                        str(before.premium_tier),
                        str(after.premium_tier),
                        str(before.premium_subscription_count),
                        str(after.premium_subscription_count),
                    ),
                )
            if before.system_channel != after.system_channel:
                case_embed.add_field(
                    name="Server System Channel Changed",
                    value="{} ➡️ {}".format(
                        str(
                            before.system_channel.mention
                            if before.system_channel
                            else before.system_channel
                        ),
                        str(
                            after.system_channel.mention
                            if after.system_channel
                            else after.system_channel
                        ),
                    ),
                )
            if before.owner != after.owner:
                case_embed.add_field(
                    name="Server Owner Changed",
                    value="{} ➡️ {}".format(before.owner.mention, after.owner.mention),
                )
        elif self.action == case_action.ROLE_EDITED:
            diffs = []
            before: discord.Role = self.data["payload"][0]
            after: discord.Role = self.data["payload"][1]
            if before.name != after.name:
                diffs.append(
                    "{} ➡️ {}\n".format(
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=before.name)
                        ),
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=after.name)
                        ),
                    )
                )
            if before.hoist != after.hoist:
                diffs.append("Hoisted: `{}` ➡️ `{}`\n".format(str(before.hoist), str(after.hoist)))
            if before.position != after.position:
                diffs.append(
                    "Position: `{}` ➡️ `{}`\n".format(str(before.position), str(after.position))
                )
            if before.managed != after.managed:
                diffs.append(
                    "Managed: `{}` ➡️ `{}`\n".format(str(before.managed), str(after.managed))
                )
            if before.mentionable != after.mentionable:
                diffs.append(
                    "Mentionable: `{}` ➡️ `{}`\n".format(
                        str(before.mentionable), str(after.mentionable)
                    )
                )
            if before.color != after.color:
                diffs.append(
                    "Color: `{}` ➡️ `{}`\n".format(
                        str(before.color),
                        str(after.color),
                    )
                )
            diffs_output = "\n".join(diffs)
            if (
                not (diffs_output.count("\n") < 2 and "Position:" in diffs_output)
                and len(diffs_output) != 0
            ):
                case_embed.add_field(
                    name="Server Role {} Changed".format(
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=before.name)
                        )
                    ),
                    value="{}".format(diffs_output),
                )
            if before.permissions != after.permissions:
                output = []
                before_permissions = []
                after_permissions = []

                for permission, boolean in iter(before.permissions):
                    before_permissions.append(
                        (permission.replace("_", " ").title().replace("Guild", "Server"), boolean)
                    )
                for permission, boolean in iter(after.permissions):
                    after_permissions.append(
                        (permission.replace("_", " ").title().replace("Guild", "Server"), boolean)
                    )

                diffs = self.cog.core.listDiff(before=before_permissions, after=after_permissions)

                if diffs.added:
                    for permission, boolean in diffs.added:
                        output.append(
                            "{}: {} ➡️ {}".format(
                                permission,
                                str(not boolean),
                                str(boolean),
                            )
                        )

                output_text = "\n".join(output)
                if len(output_text) < 1000:
                    case_embed.add_field(
                        name="{} Permissions Changed".format(
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(text=after.name)
                            )
                        ),
                        value=output_text,
                        inline=False,
                    )
                else:
                    case_embed.add_field(
                        name="{} Permissions Changed (Compressed)".format(
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(text=after.name)
                            )
                        ),
                        value="```\n{}\n```".format(
                            base64.b64encode(
                                bz2.compress(output_text.encode(), compresslevel=9)
                            ).decode(),
                        ),
                        inline=False,
                    )
                    case_embed.add_field(
                        name="Decompression Instructions",
                        value=(
                            "1. Copy data inside code block\n"
                            "2. Use command `/serverlog decompress` to decompress data\n"
                            "3. Paste data into command `input` argument\n"
                        ),
                        inline=True,
                    )
        elif self.action == case_action.ROLE_CREATED or self.action == case_action.ROLE_DELETED:
            role: discord.Role = self.data["payload"][0]
            role_permissions = []
            for permission, boolean in iter(role.permissions):
                role_permissions.append(
                    (permission.replace("_", " ").title().replace("Guild", "Server"), boolean)
                )
            data = ""
            data += "Hoisted: `{}`\n".format(str(role.hoist))
            data += "Position: `{}`\n".format(str(role.position))
            data += "Managed: `{}`\n".format(str(role.managed))
            data += "Mentionable: `{}`\n".format(str(role.mentionable))
            data += "Color: `{}`\n".format(
                str(role.color),
            )
            case_embed.add_field(
                name="{}: {}".format(
                    self.action.name.replace("_", " ").title(),
                    discord.utils.escape_markdown(
                        text=discord.utils.escape_mentions(text=role.name)
                    ),
                ),
                value=data,
            )
            permissions_list = []
            for permission, boolean in role_permissions:
                if not boolean:
                    continue
                permissions_list.append(permission)
            if permissions_list:
                output = self.cog.core.naturalize.list.listContents(
                    permissions_list, truncate=True
                )
                if len(output) < 1000:
                    case_embed.add_field(
                        name="{} Permissions".format(
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(text=role.name)
                            )
                        ),
                        value=output,
                    )
                else:
                    case_embed.add_field(
                        name="{} Permissions (Compressed)".format(
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(text=role.name)
                            )
                        ),
                        value="```\n{}\n```".format(
                            base64.b64encode(
                                bz2.compress(output.encode(), compresslevel=9)
                            ).decode(),
                        ),
                        inline=False,
                    )
                    case_embed.add_field(
                        name="Decompression Instructions",
                        value=(
                            "1. Copy data inside code block\n"
                            "2. Use command `/serverlog decompress` to decompress data\n"
                            "3. Paste data into command `input` argument\n"
                        ),
                        inline=True,
                    )
        elif self.action == case_action.EMOJI_EDITED:
            diffs = self.cog.core.listDiff(
                before=self.data["payload"][0], after=self.data["payload"][1]
            )

            if not diffs.after and not diffs.before:
                raise NotImplementedError("tried to log something, but nothing actually changed?")

            added = []
            if diffs.after:
                for item in diffs.after:
                    added.append("{} `({})`".format(item.name, item.id))

            removed = []
            if diffs.before:
                for item in diffs.before:
                    removed.append("{} `({})`".format(item.name, item.id))

            _n = "\n"

            case_embed.add_field(
                name="Server Emoji List Changed",
                value=(
                    f"{'Removed: ' if diffs.before else ''}"
                    f"{self.cog.core.naturalize.list.listContents(removed) if diffs.before else ''}"
                    f"{_n if diffs.before and diffs.after else ''}"
                    f"{'Added: ' if diffs.after else ''}"
                    f"{self.cog.core.naturalize.list.listContents(added) if diffs.after else ''}"
                ),
            )

        if len(case_embed.fields) == 1 and case_embed.fields[0].name == "Log Info":
            case_embed.add_field(
                name="⚠️ Nothing Logged!",
                value=(
                    "Please find a corresponding Audit Log entry "
                    "and bring this incident to the attention "
                    "of a LavCorps-Line developer."
                ),
            )

        return case_embed

    async def make_embed_channel(self, case_embed: discord.Embed) -> discord.Embed:
        """

        Args:
            case_embed:

        Returns:

        """
        case_embed.set_footer(
            text="Guild ID: {}\nChannel ID: {}".format(
                str(self.data["payload"][0].guild.id),
                str(self.data["payload"][0].id),
            )
        )
        case_embed.add_field(
            name="Log Info",
            value="{} `{}{}` has been {}.".format(
                (
                    "Category"
                    if isinstance(self.data["payload"][0], discord.CategoryChannel)
                    else "Channel"
                ),
                "#" if isinstance(self.data["payload"][0], discord.TextChannel) else "",
                discord.utils.escape_markdown(
                    text=discord.utils.escape_mentions(text=self.data["payload"][0].name)
                ),
                self.type.verb,
            ),
            inline=False,
        )

        if self.action == case_action.CHANNEL_EDITED:
            before: discord.abc.GuildChannel = self.data["payload"][0]
            after: discord.abc.GuildChannel = self.data["payload"][1]
            data = ""
            if before.name != after.name:
                data += "Name: `{}` ➡️ `{}`\n".format(
                    discord.utils.escape_markdown(
                        text=discord.utils.escape_mentions(text=before.name)
                    ),
                    discord.utils.escape_markdown(
                        text=discord.utils.escape_mentions(text=after.name)
                    ),
                )
            # if before.position != after.position:
            #    data += "Position: `{}` ➡️ `{}`\n".format(
            #        str(before.position + 1), str(after.position + 1))

            if isinstance(before, discord.CategoryChannel):
                case_embed.title = case_embed.title.replace("Channel", "Category")
            else:
                if (
                    before.category != after.category
                    or before.permissions_synced != after.permissions_synced
                ):
                    data += "{}{}".format(
                        (
                            "Category: `{}` ➡️ `{}`\n".format(
                                before.category.name if before.category else str(None),
                                after.category.name if after.category else str(None),
                            )
                            if before.category != after.category
                            else ""
                        ),
                        (
                            "Permissions Synced to Category: `{}` ➡️ `{}`\n".format(
                                str(before.permissions_synced),
                                str(after.permissions_synced),
                            )
                            if before.permissions_synced != after.permissions_synced
                            else ""
                        ),
                    )
            overwriteUpdateKeys = []
            overwriteXBeforeKeys = []
            overwriteXAfterKeys = []
            for key in before.overwrites:
                if key in after.overwrites.keys():
                    overwriteUpdateKeys.append(key)
                else:
                    overwriteXBeforeKeys.append(key)
            for key in after.overwrites:
                if key not in before.overwrites.keys():
                    overwriteXAfterKeys.append(key)
            if overwriteUpdateKeys:
                for update in overwriteUpdateKeys:
                    xupdates = ""
                    for permName, permValue in iter(after.overwrites[update]):
                        for permNameAlt, permValueAlt in iter(before.overwrites[update]):
                            if permName == permNameAlt:
                                if hash(permValue) == hash(permValueAlt) and permValue is None:
                                    pass
                                elif hash(permValue) == hash(permValueAlt):
                                    pass
                                else:
                                    xupdates += "{}: `{}` ➡️ `{}`\n".format(
                                        permName.replace("_", " ")
                                        .title()
                                        .replace("Guild", "Server"),
                                        str(permValueAlt),
                                        str(permValue),
                                    )
                    if xupdates != "":
                        case_embed.add_field(
                            name="Updated Permission Overwrites For {} in {}{}".format(
                                discord.utils.escape_markdown(
                                    text=discord.utils.escape_mentions(text=update.name)
                                ),
                                "#" if isinstance(before, discord.TextChannel) else "",
                                discord.utils.escape_markdown(
                                    text=discord.utils.escape_mentions(text=before.name)
                                ),
                            ),
                            value=xupdates,
                        )
            if overwriteXBeforeKeys:
                for update in overwriteXBeforeKeys:
                    xbefore = ""
                    for permName, permValue in iter(before.overwrites[update]):
                        if permValue is None:
                            pass
                        else:
                            xbefore += "{}: `{}`\n".format(
                                permName.replace("_", " ").title().replace("Guild", "Server"),
                                str(permValue),
                            )
                    case_embed.add_field(
                        name="Removed Permission Overwrites For {} in {}{}".format(
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(text=update.name)
                            ),
                            "#" if isinstance(before, discord.TextChannel) else "",
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(text=before.name)
                            ),
                        ),
                        value=(
                            xbefore
                            if xbefore != ""
                            else "None, overwrite removed with no overwritten permissions."
                        ),
                    )
            if overwriteXAfterKeys:
                for update in overwriteXAfterKeys:
                    xafter = ""
                    for permName, permValue in iter(after.overwrites[update]):
                        if permValue is None:
                            pass
                        else:
                            xafter += "{}: `{}`\n".format(
                                permName.replace("_", " ").title().replace("Guild", "Server"),
                                str(permValue),
                            )
                    case_embed.add_field(
                        name="Added Permission Overwrites For {} in {}{}".format(
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(text=update.name)
                            ),
                            "#" if isinstance(before, discord.TextChannel) else "",
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(text=before.name)
                            ),
                        ),
                        value=(
                            xafter
                            if xafter != ""
                            else "None, overwrite introduced with no overwritten permissions."
                        ),
                    )
            if isinstance(before, discord.TextChannel):
                after: discord.TextChannel
                if before.slowmode_delay != after.slowmode_delay:
                    data += "Slow mode Delay: `{}` ➡️ `{}`".format(
                        str(before.slowmode_delay), str(after.slowmode_delay)
                    )
                if not (
                    (before.topic is None or before.topic == "")
                    and (after.topic is None or after.topic == "")
                ):
                    if before.nsfw != after.nsfw:
                        data += "Marked NSFW: `{}` ➡️ `{}`".format(
                            str(before.nsfw), str(after.nsfw)
                        )
                    if before.topic != after.topic:
                        case_embed.add_field(
                            name="{} `{}{}`'s Old Description".format(
                                (
                                    "Category"
                                    if isinstance(before, discord.CategoryChannel)
                                    else "Channel"
                                ),
                                "#" if isinstance(before, discord.TextChannel) else "",
                                discord.utils.escape_markdown(
                                    text=discord.utils.escape_mentions(text=before.name)
                                ),
                            ),
                            value=(
                                "```\n{}\n```".format(
                                    discord.utils.escape_markdown(
                                        text=discord.utils.escape_mentions(text=before.topic)
                                    )
                                )
                                if before.topic
                                else str(None)
                            ),
                        )
                        case_embed.add_field(
                            name="{} `{}{}`'s New Description".format(
                                (
                                    "Category"
                                    if isinstance(before, discord.CategoryChannel)
                                    else "Channel"
                                ),
                                "#" if isinstance(before, discord.TextChannel) else "",
                                discord.utils.escape_markdown(
                                    text=discord.utils.escape_mentions(text=before.name)
                                ),
                            ),
                            value=(
                                "```\n{}\n```".format(
                                    discord.utils.escape_markdown(
                                        text=discord.utils.escape_mentions(text=after.topic)
                                    )
                                )
                                if after.topic
                                else str(None)
                            ),
                        )

            elif isinstance(before, discord.VoiceChannel) or isinstance(
                before, discord.StageChannel
            ):
                after: discord.VoiceChannel | discord.StageChannel
                if (
                    before.bitrate != after.bitrate
                    and (before.bitrate is None or before.bitrate == 0)
                    and (after.bitrate is None or after.bitrate == 0)
                ):
                    data += "Bitrate: `{}` ➡️ `{}`".format(str(before.bitrate), str(after.bitrate))
                if (
                    before.user_limit != after.user_limit
                    and (before.user_limit is None or before.user_limit == 0)
                    and (after.user_limit is None or after.user_limit == 0)
                ):
                    data += "User Limit: `{}` ➡️ `{}`".format(
                        str(before.user_limit), str(after.user_limit)
                    )

            if data and not (data.count("\n") < 2 and "Position:" in data):
                case_embed.add_field(
                    name="{} Info".format(
                        "Category" if isinstance(before, discord.CategoryChannel) else "Channel"
                    ),
                    value=data,
                    inline=True,
                )

        else:
            channel: discord.abc.GuildChannel = self.data["payload"][0]
            data = ""
            data += "Type: {}\n".format(
                "Category"
                if isinstance(channel, discord.CategoryChannel)
                else (
                    "Text Channel"
                    if isinstance(channel, discord.TextChannel)
                    else (
                        "Voice Channel"
                        if isinstance(channel, discord.VoiceChannel)
                        else (
                            "Stage Channel"
                            if isinstance(channel, discord.StageChannel)
                            else (
                                "Forum Channel"
                                if isinstance(channel, discord.ForumChannel)
                                else "⚠️ Unknown Channel Type!"
                            )
                        )
                    )
                )
            )
            data += "Position: `{}`\n".format(str(channel.position + 1))
            if not isinstance(channel, discord.CategoryChannel):
                if hasattr(channel.category, "name"):
                    data += "Category: `{}`\n".format(
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=str(channel.category.name))
                        )
                    )
                    data += "Permissions Synced to Category: `{}`\n".format(
                        str(channel.permissions_synced)
                    )
                else:
                    data += "Category: `None`\n"
            else:
                case_embed.title = case_embed.title.replace("Channel", "Category")
            case_embed.add_field(
                name="{} Data".format(
                    "Channel" if not isinstance(channel, discord.CategoryChannel) else "Category"
                ),
                value=data,
            )
            for overwriteKey, overwriteValue in channel.overwrites.items():
                overwrites = ""
                permissions = [perm for perm, boolean in iter(overwriteValue)]
                for index, permStr in enumerate(permissions):
                    permissions[index] = (
                        permStr.replace("_", " ").title().replace("Guild", "Server")
                    )
                overwritten = [boolean for perm, boolean in iter(overwriteValue)]
                for index, permStr in enumerate(permissions):
                    if overwritten[index] is not None:
                        overwrites += "{}: `{}`\n".format(permStr, str(overwritten[index]))
                case_embed.add_field(
                    name="Permission Overwrites For `{}`".format(
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=overwriteKey.name)
                        )
                    ),
                    value=(
                        overwrites
                        if overwrites != ""
                        else "None, overwrite {}d without specified overwrites.".format(
                            self.action
                        )
                    ),
                )

        if len(case_embed.fields) == 1 and case_embed.fields[0].name == "Log Info":
            case_embed.add_field(
                name="⚠️ Nothing Logged!",
                value=(
                    "Please find a corresponding Audit Log entry "
                    "and bring this incident to the attention "
                    "of a LavCorps-Line developer."
                ),
            )

        return case_embed

    async def make_embed_message(self, case_embed: discord.Embed) -> discord.Embed:
        """

        Args:
            case_embed:

        Returns:

        """
        if (
            self.action == case_action.MESSAGE_EDITED
            and self.data["payload"][0].content == self.data["payload"][1].content
        ):
            # shouldn't we be checking for this in the listener?
            raise NotImplementedError("edit event had identical content")

        case_embed.set_footer(
            text="Guild ID: {}\nMessage ID: {}\nAuthor ID: {}".format(
                str(self.guild.id),
                str(self.data["payload"][0].id),
                str(self.data["payload"][0].author.id),
            )
        )

        case_embed.add_field(
            name="Log Info",
            value="Message by {} `{}{}` in {} has been {}.".format(
                self.data["payload"][0].author.mention,
                discord.utils.escape_markdown(
                    text=discord.utils.escape_mentions(text=self.data["payload"][0].author.name)
                ),
                (
                    "#" + self.data["payload"][0].author.discriminator
                    if self.data["payload"][0].author.bot
                    else ""
                ),
                self.data["payload"][0].channel.mention,
                self.type.verb,
            ),
            inline=False,
        )

        if self.action == case_action.MESSAGE_DELETED:
            if len(self.data["payload"][0].content) < 1000:
                case_embed.add_field(
                    name="Message Text",
                    value=(
                        self.data["payload"][0].content
                        if self.data["payload"][0].content
                        else "Message had no contents"
                    ),
                    inline=True,
                )
            else:
                case_embed.add_field(
                    name="Message Text (Compressed)",
                    value="```\n{}\n```".format(
                        base64.b64encode(
                            bz2.compress(self.data["payload"][0].content.encode(), compresslevel=9)
                        ).decode()
                    ),
                    inline=False,
                )
        else:
            if (
                len(self.data["payload"][0].content) < 1000
                and len(self.data["payload"][1].content) < 1000
            ):
                case_embed.add_field(
                    name="Old Message Text",
                    value=(
                        self.data["payload"][0].content
                        if self.data["payload"][0].content
                        else "Message had no contents"
                    ),
                    inline=True,
                )
                case_embed.add_field(
                    name="New Message Text",
                    value=(
                        self.data["payload"][1].content
                        if self.data["payload"][1].content
                        else "Message has no contents"
                    ),
                    inline=True,
                )
            else:
                case_embed.add_field(
                    name="Old Message Text (Compressed)",
                    value="```\n{}\n```".format(
                        base64.b64encode(
                            bz2.compress(self.data["payload"][0].content.encode(), compresslevel=9)
                        ).decode()
                    ),
                    inline=False,
                )
                case_embed.add_field(
                    name="New Message Text (Compressed)",
                    value="```\n{}\n```".format(
                        base64.b64encode(
                            bz2.compress(self.data["payload"][1].content.encode(), compresslevel=9)
                        ).decode()
                    ),
                    inline=False,
                )

        if len(self.data["payload"][0].content) >= 1000:
            case_embed.add_field(
                name="Decompression Instructions",
                value=(
                    "1. Copy data inside code block\n"
                    "2. Use command `/serverlog decompress` to decompress data\n"
                    "3. Paste data into command `input` argument\n"
                ),
                inline=True,
            )

        return case_embed

    async def make_embed_user(self, case_embed: discord.Embed) -> discord.Embed:
        """

        Args:
            case_embed:

        Returns:

        """
        timestamp = self.timestamp

        case_embed.set_footer(
            text="Guild ID: {}\nUser ID: {}".format(
                str(self.guild.id), str(self.data["payload"][0].id)
            )
        )

        if (
            self.action == case_action.USER_BANNED
            or self.action == case_action.USER_UNBANNED
            or self.action == case_action.MEMBER_JOINED
            or self.action == case_action.MEMBER_LEFT
        ):
            if self.action == case_action.USER_BANNED:
                action = "been banned from"
            elif self.action == case_action.USER_UNBANNED:
                action = "been unbanned from"
            elif self.action == case_action.MEMBER_JOINED:
                action = "joined"
            elif self.action == case_action.MEMBER_LEFT:
                action = "left"
            else:
                action = str(None)
            case_embed.add_field(
                name="User `{}{}` has {} `{}`.".format(
                    discord.utils.escape_markdown(
                        text=discord.utils.escape_mentions(text=self.data["payload"][0].name)
                    ),
                    (
                        "#" + self.data["payload"][0].discriminator
                        if self.data["payload"][0].bot
                        else ""
                    ),
                    action,
                    discord.utils.escape_markdown(
                        text=discord.utils.escape_mentions(text=self.guild.name)
                    ),
                ),
                value="User: {} `{}{}`\n{}\nUser Snowflake ID: `{}`\nServer Member Count: `{}`".format(
                    self.data["payload"][0].mention,
                    discord.utils.escape_markdown(
                        text=discord.utils.escape_mentions(text=self.data["payload"][0].name)
                    ),
                    (
                        "#" + self.data["payload"][0].discriminator
                        if self.data["payload"][0].bot
                        else ""
                    ),
                    (
                        "User Creation Date: `{} - {} ago`".format(
                            self.data["payload"][0].created_at.strftime("%b %d, %Y"),
                            str(
                                humanize.naturaldelta(
                                    timestamp - self.data["payload"][0].created_at
                                )
                            ),
                        )
                        if self.action == case_action.MEMBER_JOINED
                        else ""
                    ),
                    str(self.data["payload"][0].id),
                    str(self.guild.member_count),
                ),
            )
            del action
        if self.action == case_action.MEMBER_LEFT:
            if self.data["payload"][0].nick:
                case_embed.add_field(
                    name="User Nickname",
                    value=discord.utils.escape_markdown(
                        text=discord.utils.escape_mentions(text=self.data["payload"][0].nick)
                    ),
                )
            case_embed.add_field(
                name="User Roles",
                value=self.cog.core.naturalize.list.listContents(
                    [
                        discord.utils.escape_markdown(
                            text=discord.utils.escape_mentions(text=role.name)
                        )
                        for role in self.data["payload"][0].roles
                    ]
                ),
            )
        if self.action == case_action.USER_UPDATED:
            case_embed.add_field(
                name="User `{}{}` Changed".format(
                    discord.utils.escape_markdown(
                        text=discord.utils.escape_mentions(text=self.data["payload"][0].name)
                    ),
                    (
                        "#" + self.data["payload"][0].discriminator
                        if self.data["payload"][0].bot
                        else ""
                    ),
                ),
                value="{}{}{}".format(
                    (
                        "Name: `{}` ➡️ `{}`\n".format(
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(
                                    text=self.data["payload"][0].name
                                )
                            ),
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(
                                    text=self.data["payload"][1].name
                                )
                            ),
                        )
                        if self.data["payload"][0].name != self.data["payload"][1].name
                        else ""
                    ),
                    (
                        "Avatar: {} ➡️ {}\n".format(
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(
                                    text=str(self.data["payload"][0].display_avatar.url)
                                )
                            ),
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(
                                    text=str(self.data["payload"][1].display_avatar.url)
                                )
                            ),
                        )
                        if self.data["payload"][0].display_avatar.url
                        != self.data["payload"][1].display_avatar.url
                        else ""
                    ),
                    (
                        "Discriminator: `#{}` ➡️ `#{}`\n".format(
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(
                                    text=self.data["payload"][0].discriminator
                                )
                            ),
                            discord.utils.escape_markdown(
                                text=discord.utils.escape_mentions(
                                    text=self.data["payload"][1].discriminator
                                )
                            ),
                        )
                        if self.data["payload"][0].discriminator
                        != self.data["payload"][1].discriminator
                        else ""
                    ),
                ),
            )
        if self.action == case_action.MEMBER_UPDATED:
            diffs = self.cog.core.listDiff(
                self.data["payload"][0].roles, self.data["payload"][1].roles
            )
            case_embed.add_field(
                name="Member `{}{}` Changed".format(
                    discord.utils.escape_markdown(
                        text=discord.utils.escape_mentions(text=self.data["payload"][0].name)
                    ),
                    (
                        "#" + self.data["payload"][0].discriminator
                        if self.data["payload"][0].bot
                        else ""
                    ),
                ),
                value="{}{}{}".format(
                    (
                        "Nickname: `{}` ➡️ `{}`\n".format(
                            (
                                discord.utils.escape_markdown(
                                    text=discord.utils.escape_mentions(
                                        text=self.data["payload"][0].nick
                                    )
                                )
                                if self.data["payload"][0].nick
                                else "None"
                            ),
                            (
                                discord.utils.escape_markdown(
                                    text=discord.utils.escape_mentions(
                                        text=self.data["payload"][1].nick
                                    )
                                )
                                if self.data["payload"][1].nick
                                else "None"
                            ),
                        )
                        if self.data["payload"][0].nick != self.data["payload"][1].nick
                        else ""
                    ),
                    (
                        "Added Roles: {}\n".format(
                            self.cog.core.naturalize.list.listContents(
                                [role.name for role in diffs.added]
                            )
                        )
                        if diffs.added
                        else ""
                    ),
                    (
                        "Removed Roles: {}\n".format(
                            self.cog.core.naturalize.list.listContents(
                                [role.name for role in diffs.removed]
                            )
                        )
                        if diffs.removed
                        else ""
                    ),
                ),
            )

        if self.action == case_action.USER_BANNED:
            banList = [x async for x in self.data["payload"][1].bans()]
            userBanEntry = None
            for ban in banList:
                if ban.user == self.data["payload"][0]:
                    userBanEntry = ban
                    break
            if userBanEntry is None:
                banReason = "N/A"
            elif userBanEntry.reason is None:
                banReason = "N/A"
            else:
                banReason = userBanEntry.reason

            case_embed.add_field(name="Ban Reason", value=banReason)

        return case_embed

    async def make_embed_invite(self, case_embed: discord.Embed) -> discord.Embed:
        """

        Args:
            case_embed:

        Returns:

        """
        case_embed.set_footer(
            text="Guild ID: {}\nInvite ID: {}\nAuthor ID: {}".format(
                str(self.guild.id),
                str(self.data["payload"][0].id),
                str(
                    self.data["payload"][0].inviter.id
                    if self.data["payload"][0].inviter
                    else "Unavailable"
                ),
            )
        )

        if self.data["payload"][0].inviter:
            case_embed.add_field(
                name="Log Info",
                value=(
                    f"Invitation by {self.data['payload'][0].inviter.mention} "
                    f"`{self.data['payload'][0].inviter.name}"
                    f"{'#' + self.data['payload'][0].inviter.discriminator if self.data['payload'][0].inviter.bot else ''}`"
                    f" has been {self.type.verb}."
                ),
                inline=False,
            )
        else:
            case_embed.add_field(
                name="Log Info",
                value=f"Invitation has been {self.type.verb}.",
                inline=False,
            )

        case_embed.add_field(
            name="Invite Info",
            value=(
                f"Revoked: "
                f"{self.data['payload'][0].revoked if self.data['payload'][0].revoked else False}\n"
                f"Temporary: "
                f"{self.data['payload'][0].temporary if self.data['payload'][0].temporary else False}\n"
                f"Uses: "
                f"{self.data['payload'][0].uses if self.data['payload'][0].uses else 0}\n"
                f"Max Uses: "
                f"{self.data['payload'][0].max_uses if self.data['payload'][0].max_uses else '∞'}\n"
                f"Expires: "
                f"{self.cog.core.naturalize.time.naturaltime(self.data['payload'][0].expires_at) if self.data['payload'][0].expires_at else 'Never'}"
            ),
        )

        return case_embed


case_types: dict[case_action, case_type] = {
    case_action.USER_BANNED: case_type(
        name="User Banned",
        emoji="🔨",
        color=discord.Colour.dark_red(),
        involves=discord.User,
        verb="banned",
    ),
    case_action.USER_UNBANNED: case_type(
        name="User Unbanned",
        emoji="🕊",
        color=discord.Colour.greyple(),
        involves=discord.User,
        verb="unbanned",
    ),
    case_action.MESSAGE_EDITED: case_type(
        name="Message Edited",
        emoji="📝",
        color=discord.Colour.purple(),
        involves=discord.Message,
        verb="edited",
    ),
    case_action.MESSAGE_DELETED: case_type(
        name="Message Deleted",
        emoji="♻",
        color=discord.Colour.orange(),
        involves=discord.Message,
        verb="deleted",
    ),
    case_action.CHANNEL_EDITED: case_type(
        name="Channel Edited",
        emoji="📰",
        color=discord.Colour.dark_gold(),
        involves=discord.abc.GuildChannel,
        verb="edited",
    ),
    case_action.CHANNEL_DELETED: case_type(
        name="Channel Deleted",
        emoji="⚒",
        color=discord.Colour.magenta(),
        involves=discord.abc.GuildChannel,
        verb="deleted",
    ),
    case_action.CHANNEL_CREATED: case_type(
        name="Channel Created",
        emoji="🛠",
        color=discord.Colour.gold(),
        involves=discord.abc.GuildChannel,
        verb="created",
    ),
    case_action.GUILD_EDITED: case_type(
        name="Server Edited",
        emoji="🌐",
        color=discord.Colour.blurple(),
        involves=discord.Guild,
        verb="edited",
    ),
    case_action.MEMBER_JOINED: case_type(
        name="User Joined",
        emoji="📥",
        color=discord.Colour.green(),
        involves=discord.User,
        verb="joined",
    ),
    case_action.MEMBER_LEFT: case_type(
        name="User Left",
        emoji="📤",
        color=discord.Colour.blue(),
        involves=discord.User,
        verb="left",
    ),
    case_action.MEMBER_UPDATED: case_type(
        name="Member Changed",
        emoji="👥",
        color=discord.Colour.teal(),
        involves=discord.User,
        verb="updated",
    ),
    case_action.USER_UPDATED: case_type(
        name="User Changed",
        emoji="👤",
        color=discord.Colour.dark_teal(),
        involves=discord.User,
        verb="updated",
    ),
    case_action.ROLE_CREATED: case_type(
        name="Role Created",
        emoji="🪪",
        color=discord.Colour.gold(),  # TODO: Assign unique color
        involves=discord.Guild,
        verb="created",
    ),
    case_action.ROLE_DELETED: case_type(
        name="Role Deleted",
        emoji="🗑️",
        color=discord.Colour.magenta(),  # TODO: Assign unique color
        involves=discord.Guild,
        verb="deleted",
    ),
    case_action.ROLE_EDITED: case_type(
        name="Role Edited",
        emoji="📋",
        color=discord.Colour.dark_gold(),  # TODO: Assign unique color
        involves=discord.Guild,
        verb="edited",
    ),
    case_action.EMOJI_EDITED: case_type(
        name="Emojis Updated",
        emoji="🖊️",
        color=discord.Colour.dark_gold(),  # TODO: Assign unique color
        involves=discord.Guild,
        verb="updated",
    ),
    case_action.INVITE_CREATED: case_type(
        name="Invite Created",
        emoji="✉",
        color=discord.Color.magenta(),  # TODO: Assign unique color
        involves=discord.Invite,
        verb="created",
    ),
    case_action.INVITE_DELETED: case_type(
        name="Invite Deleted",
        emoji="✉",
        color=discord.Color.dark_magenta(),  # TODO: Assign unique color
        involves=discord.Invite,
        verb="deleted",
    ),
}


class tCfgToggle(discord.ui.Button):
    def __init__(
        self,
        style: discord.ButtonStyle,
        citem: tuple[case_action, case_type],
        callback: typing.Callable[
            [discord.Interaction, tuple[case_action, case_type]], typing.Awaitable[None]
        ],
    ):
        super().__init__(style=style, label=citem[1].name, emoji=citem[1].emoji)
        self._citem = citem
        self._callback = callback

    async def callback(self, interaction: discord.Interaction):
        await self._callback(interaction, self._citem)


class tCfgExit(discord.ui.Button):
    def __init__(
        self,
        callback: typing.Callable[
            [discord.Interaction, tuple[case_action, case_type] | None], typing.Awaitable[None]
        ],
    ):
        super().__init__(style=discord.ButtonStyle.blurple, label="Apply", emoji="💾")
        self._callback = callback

    async def callback(self, interaction: discord.Interaction):
        await self._callback(interaction, None)


class tCfg(discord.ui.View):
    """UI class for Serverlog Toggle Configurations"""

    def __init__(
        self,
        callback: typing.Callable[
            [discord.Interaction, tuple[case_action, case_type]], typing.Awaitable[None]
        ],
        toggles: dict[str, bool],
    ):
        super().__init__(timeout=180)
        self._callback = callback
        self._toggles = toggles

        for citem in case_types.items():
            self.add_item(
                tCfgToggle(
                    (
                        discord.ButtonStyle.grey
                        if citem[0].name not in toggles
                        else (
                            discord.ButtonStyle.green
                            if toggles[citem[0].name]
                            else discord.ButtonStyle.red
                        )
                    ),
                    citem,
                    self._callback,
                )
            )
        self.add_item(tCfgExit(self._callback))


class tCfgState:
    """Toggle Configuration State Tracker"""

    def __init__(self, start_time: float):
        self.lock = asyncio.Lock()
        self.modified = False
        self.exiting = False
        self.start = start_time
        self.expire = start_time + 180


class serverlog(eris.cog.Cog):
    """Bot-powered log of server events"""

    def __init__(self, **kwargs):
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.core: libCore = kwargs["deps"]["libCore"]
        self.config: libConfig = kwargs["deps"]["libConfig"]
        self.log = logger("eRis.cogs.serverlog")

    @commands.hybrid_group()
    @commands.has_guild_permissions(manage_guild=True)
    @commands.guild_only()
    async def serverlog(self, ctx):
        """Serverlog Configuration"""
        pass

    @serverlog.command()
    async def config(self, ctx: commands.Context):
        """View serverlog configuration"""
        async with lockManager(snf=ctx.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                data["channels"] = {}
            if "bot_logging" not in data:
                data["bot_logging"] = False

            serverlog_embed = discord.Embed(
                title="Serverlog Configuration", color=discord.Color.random()
            )

            for channel_id, toggles in data["channels"].items():
                toggles: dict[str, bool]
                if not ctx.guild.get_channel(int(channel_id)):
                    continue
                serverlog_embed.add_field(
                    name="Enabled log types for #{}".format(
                        ctx.guild.get_channel(int(channel_id)).name
                    ),
                    value=(
                        self.core.naturalize.list.listContents(
                            [
                                k.replace("_", " ").title() if v else None
                                for k, v in toggles.items()
                            ],
                            truncate=True,
                        )
                        if toggles
                        else "Channel has no toggles enabled"
                    ),
                )

            serverlog_embed.add_field(
                name="Server-wide Settings",
                value="Bot action logging: {}".format(
                    "ON" if data["bot_logging"] else "OFF",
                ),
            )

            await ctx.send(embed=serverlog_embed)

    @serverlog.command()
    async def channel(self, ctx: commands.Context, log_channel: discord.TextChannel):
        """Add/remove a channel to/from the Serverlog destination list."""
        async with lockManager(snf=ctx.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                data["channels"] = {}
            if "bot_logging" not in data:
                data["bot_logging"] = False

            if str(log_channel.id) in data["channels"]:  # TODO: Rewrite response output
                data["channels"].pop(str(log_channel.id))
                await ctx.send(
                    "Successfully removed {} from the serverlog destination list".format(
                        log_channel.mention
                    )
                )
            else:
                data["channels"][str(log_channel.id)] = {}
                for cact in case_types:
                    data["channels"][str(log_channel.id)][cact.name] = False
                await ctx.send(
                    "Successfully added {} to the serverlog destination list".format(
                        log_channel.mention
                    )
                )

    @serverlog.command()
    async def logging_bots(self, ctx: commands.Context, toggle: bool):
        """Set whether the Serverlog logs actions from Bot users."""
        async with lockManager(snf=ctx.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                data["channels"] = {}
            if "bot_logging" not in data:
                data["bot_logging"] = False

            data["bot_logging"] = toggle

        await ctx.send(
            "Successfully toggled bot action logging {}!".format("ON" if toggle else "OFF")
        )

    @serverlog.command()
    async def toggle(self, ctx: commands.Context, channel: discord.TextChannel):
        """Toggle logged serverlog casetypes

        You must #mention a channel to add/remove these toggles for!"""
        async with lockManager(snf=ctx.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                data["channels"] = {}
            if "bot_logging" not in data:
                data["bot_logging"] = False
            if str(channel.id) not in data["channels"]:
                data["channels"][str(channel.id)] = {}
                for cact in case_types:
                    data["channels"][str(channel.id)][cact.name] = False

            self.log.debug("setting serverlog toggles...")

            embed = discord.Embed(
                title="Serverlog Logtype Configuration",
                description=(
                    "Click on the buttons below to enable or disable events to log.\n"
                    "Click `Apply` to apply changes and exit the menu.\n"
                    "Changes will automatically be applied after three minutes."
                ),
                color=discord.Color.random(),
            )

            state = tCfgState(self.bot.loop.time())

            async def callback(
                interaction: discord.Interaction, citem: tuple[case_action, case_type] | None
            ):
                """Callback for Serverlog Toggle Configuration prompt"""
                if not citem:
                    await interaction.response.send_message(
                        "Applying changes...", ephemeral=True, delete_after=1
                    )
                    state.exiting = True
                    self.log.debug("saving serverlog toggles...")
                    return
                await state.lock.acquire()
                state.modified = True

                if citem[0].name in data["channels"][str(channel.id)]:
                    data["channels"][str(channel.id)][citem[0].name] = not data["channels"][
                        str(channel.id)
                    ][citem[0].name]
                else:
                    data["channels"][str(channel.id)][citem[0].name] = True

                if data["channels"][str(channel.id)][citem[0].name]:
                    await interaction.response.send_message(
                        f"Acknowledged, `{citem[1].name}` events will now be logged.",
                        ephemeral=True,
                        delete_after=1.5,
                    )
                else:
                    await interaction.response.send_message(
                        f"Acknowledged, `{citem[1].name}` events will no longer be logged.",
                        ephemeral=True,
                        delete_after=1.5,
                    )

                self.log.debug("serverlog toggle flipped...")
                state.lock.release()

            msg = await ctx.send(
                embed=embed,
                view=tCfg(callback, data["channels"][str(channel.id)]),
                ephemeral=True,
                delete_after=180,
            )

            while state.start < state.expire:
                if state.modified:
                    await state.lock.acquire()
                    await msg.edit(view=tCfg(callback, data["channels"][str(channel.id)]))
                    self.log.debug("updating serverlog tcfg view...")
                    state.modified = False
                    state.lock.release()
                if state.exiting:
                    await msg.delete()
                    break
                await asyncio.sleep(0)
        self.log.debug("serverlog toggles saved!")

    @serverlog.command()
    async def decompress(self, ctx: commands.Context, input: str):
        """Decompress data from Serverlog entries."""
        async with ctx.typing(ephemeral=True):
            decompressor = bz2.BZ2Decompressor()
            try:
                data = base64.b64decode(input, validate=True)
            except (binascii.Error, ValueError):
                await ctx.send("⛔ Malformed Base64 input!", ephemeral=True)
                return
            try:
                out = decompressor.decompress(data, max_length=2000)
            except (EOFError, OSError):
                await ctx.send("⛔ Malformed bzip2 input!", ephemeral=True)
                return
            try:
                out.decode(encoding="utf-8")
            except UnicodeDecodeError:
                await ctx.send("⛔ Malformed output!", ephemeral=True)
            file = discord.File(fp=io.BytesIO(initial_bytes=out), filename="output.txt")
            await ctx.send(
                content=(
                    "ℹ️ Successfully decompressed data!"
                    if decompressor.eof
                    else "⚠️ Decompressed data up to 2000 bytes. There was some data left over."
                ),
                file=file,
                ephemeral=True,
            )

    @commands.Cog.listener("on_raw_bulk_message_delete")
    async def message_deleted_bulk(self, payload: discord.RawBulkMessageDeleteEvent) -> None:
        """

        Args:
            payload:

        Returns:

        """
        self.log.debug("detected bulk message deletion")
        data = {}
        try:
            data = await self.config.read(snf=payload.guild_id, cog="serverlog")
        except eris.exceptions.DataNotFound:
            self.log.debug("no config for this server, aborting")
            return
        except eris.exceptions.ConfigOperationalError as OpErr:
            await asyncio.sleep(300)
            for i in range(1, 5):
                try:
                    data = await self.config.read(snf=payload.guild_id, cog="serverlog")
                except eris.exceptions.ConfigOperationalError:
                    await asyncio.sleep(300)
                    continue
                else:
                    break
            if not data:
                raise OpErr
        if "channels" not in data:
            self.log.debug("no channel toggles for this server, aborting")
            return
        guild = self.bot.get_guild(payload.guild_id)
        if not guild:
            return
        self.log.debug("logging bulk message deletion")
        for msg in payload.cached_messages:
            self.bot.scheduler.schedule_task(self.message_deleted(msg))

    @commands.Cog.listener("on_message_delete")
    async def message_deleted(self, message: discord.Message) -> None:
        """

        Args:
            message:

        Returns:

        """
        self.log.debug("detected message deletion")
        if not message.guild:
            self.log.debug("message deletion didn't happen in a server, aborting")
            return
        async with lockManager(snf=message.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            if not message.guild:
                self.log.debug("guild doesn't exist, aborting")
                return
            if not data["bot_logging"] and message.author.bot:
                self.log.debug("author is bot and we aren't logging bot events, aborting")
                return
            self.log.debug("logging message deletion")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.MESSAGE_DELETED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.MESSAGE_DELETED,
                    guild=message.guild,
                    data={
                        "author": message.author,
                        "channel": message.channel,
                        "payload": [message],
                    },
                    archive=[],
                    log_channels=[
                        message.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.MESSAGE_DELETED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_message_edit")
    async def message_edited(self, before: discord.Message, after: discord.Message) -> None:
        """

        Args:
            before:
            after:

        Returns:

        """
        self.log.debug("detected message edit")
        if not after.guild:
            self.log.debug("message edit didn't happen in a server, aborting")
            return

        async with lockManager(snf=before.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            if not before.guild:
                self.log.debug("guild doesn't exist, aborting")
                return
            if before.content == after.content:
                self.log.debug("content is exactly the same, aborting")
                return
            if not data["bot_logging"] and before.author.bot:
                self.log.debug("author is bot and we aren't logging bot events, aborting")
                return
            self.log.debug("logging message edit")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.MESSAGE_EDITED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.MESSAGE_EDITED,
                    guild=after.guild,
                    data={
                        "author": after.author,
                        "channel": after.channel,
                        "payload": [before, after],
                    },
                    archive=[],
                    log_channels=[
                        after.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.MESSAGE_EDITED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_guild_channel_delete")
    async def channel_deleted(self, channel: discord.abc.GuildChannel) -> None:
        """

        Args:
            channel:

        Returns:

        """
        self.log.debug("detected guild channel deletion")
        async with lockManager(snf=channel.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging guild channel deletion")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.CHANNEL_DELETED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.CHANNEL_DELETED,
                    guild=channel.guild,
                    data={"payload": [channel]},
                    archive=[],
                    log_channels=[
                        channel.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.CHANNEL_DELETED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_guild_channel_create")
    async def channel_created(self, channel: discord.abc.GuildChannel) -> None:
        """

        Args:
            channel:

        Returns:

        """
        self.log.debug("detected guild channel creation")
        async with lockManager(snf=channel.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging guild channel creation")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.CHANNEL_CREATED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.CHANNEL_CREATED,
                    guild=channel.guild,
                    data={"payload": [channel]},
                    archive=[],
                    log_channels=[
                        channel.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.CHANNEL_CREATED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_guild_channel_update")
    async def channel_update(
        self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel
    ) -> None:
        """

        Args:
            before:
            after:

        Returns:

        """
        self.log.debug("detected guild channel update")
        async with lockManager(snf=before.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging guild channel update")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.CHANNEL_EDITED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.CHANNEL_EDITED,
                    guild=after.guild,
                    data={"payload": [before, after]},
                    archive=[],
                    log_channels=[
                        after.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.CHANNEL_EDITED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_member_join")
    async def member_join(self, member: discord.Member) -> None:
        """

        Args:
            member:

        Returns:

        """
        self.log.debug("detected new server member")
        async with lockManager(snf=member.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging new server member")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.MEMBER_JOINED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.MEMBER_JOINED,
                    guild=member.guild,
                    data={"payload": [member]},
                    archive=[],
                    log_channels=[
                        member.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.MEMBER_JOINED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_member_remove")
    async def member_remove(self, member: discord.Member) -> None:
        """

        Args:
            member:

        Returns:

        """
        self.log.debug("detected server member left")
        async with lockManager(snf=member.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging server member left")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.MEMBER_LEFT],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.MEMBER_LEFT,
                    guild=member.guild,
                    data={"payload": [member]},
                    archive=[],
                    log_channels=[
                        member.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.MEMBER_LEFT.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_member_update")
    async def member_update(self, before: discord.Member, after: discord.Member) -> None:
        """

        Args:
            before:
            after:

        Returns:

        """
        self.log.debug("detected server member update")
        async with lockManager(snf=before.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            if (
                before.activities != after.activities
                or before.status != after.status
                or before.mobile_status != after.mobile_status
                or before.desktop_status != after.desktop_status
                or before.web_status != after.web_status
                or before.premium_since != after.premium_since
            ):
                return
            if (
                before.activities == after.activities
                and before.status == after.status
                and before.mobile_status == after.mobile_status
                and before.desktop_status == after.desktop_status
                and before.web_status == after.web_status
                and len(before.roles) == len(after.roles)
                and before.nick == after.nick
                and before.premium_since == after.premium_since
                and before.color == after.color
                and before.display_name == after.display_name
            ):
                return
            self.log.debug("logging server member update")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.MEMBER_UPDATED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.MEMBER_UPDATED,
                    guild=after.guild,
                    data={"payload": [before, after]},
                    archive=[],
                    log_channels=[
                        after.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.MEMBER_UPDATED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_user_update")
    async def user_update(self, before: discord.User, after: discord.User) -> None:
        """

        Args:
            before:
            after:
        """
        self.log.debug("detected user update")
        guilds_to_log = []
        for guild in self.bot.guilds:
            # if after in server.members or before in server.members:
            if guild.get_member(after.id) or guild.get_member(before.id):
                guilds_to_log.append(guild)

        for guild in guilds_to_log:
            async with lockManager(snf=guild, bot=self.bot, cog=self) as data:
                if "channels" not in data:
                    continue
                if "bot_logging" not in data:
                    data["bot_logging"] = False
                self.log.debug("logging user update")

                self.bot.scheduler.schedule_task(
                    case_data(
                        in_case_type=case_types[case_action.USER_UPDATED],
                        timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                        action=case_action.USER_UPDATED,
                        guild=guild,
                        data={"payload": [before, after]},
                        archive=[],
                        log_channels=[
                            guild.get_channel(int(channel_id))
                            for channel_id, toggles in data["channels"].items()
                            if toggles[case_action.USER_UPDATED.name]
                        ],
                        cog=self,
                    ).log()
                )

    @commands.Cog.listener("on_guild_role_create")
    async def guild_role_add(self, role: discord.Role) -> None:
        """

        Args:
            role:

        Returns:

        """
        self.log.debug("detected guild role creation")
        async with lockManager(snf=role.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging guild role creation")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.ROLE_CREATED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.ROLE_CREATED,
                    guild=role.guild,
                    data={"payload": [role]},
                    archive=[],
                    log_channels=[
                        role.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.ROLE_CREATED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_guild_role_delete")
    async def guild_role_remove(self, role: discord.Role) -> None:
        """

        Args:
            role:

        Returns:

        """
        self.log.debug("detected guild role deletion")
        async with lockManager(snf=role.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging guild role deletion")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.ROLE_DELETED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.ROLE_DELETED,
                    guild=role.guild,
                    data={"payload": [role]},
                    archive=[],
                    log_channels=[
                        role.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.ROLE_DELETED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_guild_role_update")
    async def guild_role_update(self, before: discord.Role, after: discord.Role) -> None:
        """

        Args:
            before:
            after:

        Returns:

        """
        self.log.debug("detected guild role update")
        async with lockManager(snf=before.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            if (
                before.id == after.id
                and before.name == after.name
                and before.guild == after.guild
                and before.hoist == after.hoist
                and before.position == after.position
                and before.managed == after.managed
                and before.mentionable == after.mentionable
                and before.permissions == after.permissions
                and before.color == after.color
                and before.mention == after.mention
                and before.members == after.members
            ):
                return
            self.log.debug("logging guild role update")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.ROLE_EDITED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.ROLE_EDITED,
                    guild=after.guild,
                    data={"payload": [before, after]},
                    archive=[],
                    log_channels=[
                        after.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.ROLE_EDITED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_guild_emojis_update")
    async def guild_emoji_update(
        self, guild: discord.Guild, before: discord.Emoji, after: discord.Emoji
    ) -> None:
        """

        Args:
            guild:
            before:
            after:

        Returns:

        """
        self.log.debug("detected guild emoji update")
        async with lockManager(snf=guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging guild emoji update")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.EMOJI_EDITED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.EMOJI_EDITED,
                    guild=guild,
                    data={"payload": [before, after]},
                    archive=[],
                    log_channels=[
                        guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.EMOJI_EDITED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_guild_update")
    async def guild_update(self, before: discord.Guild, after: discord.Guild) -> None:
        """

        Args:
            before:
            after:

        Returns:

        """
        self.log.debug("detected guild update")
        async with lockManager(snf=before, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging guild update")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.GUILD_EDITED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.GUILD_EDITED,
                    guild=after,
                    data={"payload": [before, after]},
                    archive=[],
                    log_channels=[
                        after.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.GUILD_EDITED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_member_ban")
    async def user_ban(self, guild: discord.Guild, user: discord.User) -> None:
        """

        Args:
            guild:
            user:

        Returns:

        """
        self.log.debug("detected member ban")
        async with lockManager(snf=guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging member ban")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.USER_BANNED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.USER_BANNED,
                    guild=guild,
                    data={"payload": [user, guild]},
                    archive=[],
                    log_channels=[
                        guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.USER_BANNED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_member_unban")
    async def user_unban(self, guild: discord.Guild, user: discord.User) -> None:
        """

        Args:
            guild:
            user:

        Returns:

        """
        self.log.debug("detected member unban")
        async with lockManager(snf=guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging member unban")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.USER_UNBANNED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.USER_UNBANNED,
                    guild=guild,
                    data={"payload": [user]},
                    archive=[],
                    log_channels=[
                        guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.USER_UNBANNED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_invite_create")
    async def invite_create(self, invite: discord.Invite) -> None:
        """

        Args:
            invite:

        Returns:

        """
        self.log.debug("detected invite creation")
        async with lockManager(snf=invite.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging invite creation")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.INVITE_CREATED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.INVITE_CREATED,
                    guild=invite.guild,
                    data={"payload": [invite]},
                    archive=[],
                    log_channels=[
                        invite.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.INVITE_CREATED.name]
                    ],
                    cog=self,
                ).log()
            )

    @commands.Cog.listener("on_invite_delete")
    async def invite_delete(self, invite: discord.Invite) -> None:
        """

        Args:
            invite:

        Returns:

        """
        self.log.debug("detected invite deletion")
        async with lockManager(snf=invite.guild, bot=self.bot, cog=self) as data:
            if "channels" not in data:
                self.log.debug("no channel toggles for this server, aborting")
                return
            if "bot_logging" not in data:
                data["bot_logging"] = False
            self.log.debug("logging invite deletion")

            self.bot.scheduler.schedule_task(
                case_data(
                    in_case_type=case_types[case_action.INVITE_DELETED],
                    timestamp=self.core.chronologize.now(tz=datetime.timezone.utc),
                    action=case_action.INVITE_DELETED,
                    guild=invite.guild,
                    data={"payload": [invite]},
                    archive=[],
                    log_channels=[
                        invite.guild.get_channel(int(channel_id))
                        for channel_id, toggles in data["channels"].items()
                        if toggles[case_action.INVITE_DELETED.name]
                    ],
                    cog=self,
                ).log()
            )
