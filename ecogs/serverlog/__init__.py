#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/welcome
## a component of eRis.Cogs
* Automatically greet users joining a server
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .serverlog import serverlog


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=serverlog(**kwargs))
