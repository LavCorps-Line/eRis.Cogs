#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/games
## a component of eRis.Cogs
* Provides miscellaneous fun commands for users
* Provides Warframe API interface for users
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .games import games


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=games(**kwargs))
