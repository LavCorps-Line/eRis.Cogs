#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/games
## a component of eRis.Cogs
* Provides miscellaneous fun commands for users
* Provides Warframe API interface for users
"""

# First-Party Imports #
import eris.bot
import eris.cog
from eris.logger import logger

rockPaperScissors = {
    "rock": "✊",
    "paper": "✋",
    "scissors": "✌️",
}

# TODO: rock paper scissors
# TODO: dice-roller 🎲 with a user-given number of sides, die, and an optional keep-highest arg
# TODO: coin-flipper


class games(eris.cog.Cog):
    """Game API introspection & fun stuff!"""

    def __init__(self, **kwargs):
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.log = logger("eRis.cogs.games")
