#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/general
## a component of eRis.Cogs
* Provides miscellaneous information about eRis
"""

# Standard Library Imports #
import asyncio
import datetime
import importlib.metadata
import sys

# Third-Party Imports #
import aiohttp
import discord
import discord.ext.commands as commands
import toml

# First-Party Imports #
import eris.bot
import eris.cog
from eris.cogs.libCore import libCore
from eris.logger import logger


class general(eris.cog.Cog):
    """General commands."""

    def __init__(self, **kwargs):
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.core: libCore = kwargs["deps"]["libCore"]
        self.log = logger("eRis.cogs.general")
        self.stopwatches = {}

    @commands.hybrid_command(name="info")
    @commands.bot_has_permissions(embed_links=True)
    async def data(self, ctx: commands.Context):
        """Shows info about Ellie"""
        author = "https://gitlab.com/LavCorps"
        org = "https://gitlab.com/LavCorps-Line"
        coreRepo = org + "/eRis"
        cogsRepo = org + "/eRis.Cogs"
        dpyRepo = "https://github.com/Rapptz/discord.py"
        pythonUrl = "https://www.python.org/"
        born = datetime.datetime.fromtimestamp(1491415624.302, tz=datetime.timezone.utc)
        dpyVersion = "[{}]({})".format(discord.__version__, dpyRepo)
        pythonVersion = "[{}.{}.{}]({})".format(*sys.version_info[:3], pythonUrl)
        try:
            coreVersion = "[{}]({})".format(importlib.metadata.version("eRis"), coreRepo)
        except importlib.metadata.PackageNotFoundError:
            coreVersion = "Custom"
        try:
            cogsVersion = "[{}]({})".format(importlib.metadata.version("ecogs"), cogsRepo)
        except importlib.metadata.PackageNotFoundError:
            cogsVersion = "Custom"
        coreLicense = "https://gitlab.com/LavCorps-Line/eRis/-/blob/main/LICENSE"
        cogsLicense = "https://gitlab.com/LavCorps-Line/eRis.Cogs/-/blob/main/LICENSE"
        appInfo = await self.bot.application_info()

        try:
            baseCore = "https://gitlab.com/LavCorps-Line/eRis/-/raw/main"
            baseCogs = "https://gitlab.com/LavCorps-Line/eRis.Cogs/-/raw/main"
            async with self.bot.httpClient.get(baseCore + "/pyproject.toml") as req:
                coreData = toml.loads((await req.content.read()).decode())
            async with self.bot.httpClient.get(baseCogs + "/pyproject.toml") as req:
                cogsData = toml.loads((await req.content.read()).decode())
        except (aiohttp.ClientError, asyncio.TimeoutError):
            coreOutdated = "Unknown"
            cogsOutdated = "Unknown"
        else:
            coreOutdated = (
                "Unknown"
                if coreVersion == "Custom"
                else (
                    "Yes, upstream version is {}".format(coreData["tool"]["poetry"]["version"])
                    if coreData["tool"]["poetry"]["version"] != importlib.metadata.version("eRis")
                    else "No"
                )
            )
            cogsOutdated = (
                "Unknown"
                if cogsVersion == "Custom"
                else (
                    "Yes, upstream version is {}".format(cogsData["tool"]["poetry"]["version"])
                    if cogsData["tool"]["poetry"]["version"] != importlib.metadata.version("ecogs")
                    else "No"
                )
            )

        embed = discord.Embed(color=discord.Color.random())
        embed.set_author(name=str(self.bot.user), icon_url=self.bot.user.avatar.url)
        if appInfo.team:
            for x in range(len(appInfo.team.members)):
                embed.add_field(
                    name="Instance Owned By{}".format(
                        " ({}/{})".format(str(x), str(len(appInfo.team.members)))
                        if len(appInfo.team.members) > 1
                        else ""
                    ),
                    value=str(appInfo.team.members[x]),
                )
        else:
            embed.add_field(name="Instance Owned By", value=str(appInfo.owner))
        embed.add_field(name="Python", value=pythonVersion)
        embed.add_field(name="discord.py", value=dpyVersion)
        embed.add_field(name="eRis version", value=coreVersion)
        embed.add_field(name="eRis.Cogs version", value=cogsVersion)
        embed.add_field(name="eRis outdated", value=coreOutdated)
        embed.add_field(name="eRis.Cogs outdated", value=cogsOutdated)
        embed.add_field(
            name="About Ellysion",
            value="This instance of eRis is the fourth major iteration of [Ellysion, a multi-purpose discord bot]({}) running [eRis.Cogs]({}) created by [LavCorps]({}) for leftist communities in need of a strong multipurpose bot with direct user support.\n\n[eRis LICENSE]({})\n[eRis.Cogs LICENSE]({})".format(
                coreRepo, cogsRepo, author, coreLicense, cogsLicense
            ),
            inline=False,
        )

        embed.set_footer(
            text="Conceived {} ({} ago!) | Born {} ({} ago!)".format(
                self.core.naturalize.time.naturaldate(born),
                self.core.naturalize.time.naturaldelta(self.core.chronologize.now() - born),
                self.core.naturalize.time.naturaldate(
                    discord.utils.snowflake_time(self.bot.user.id)
                ),
                self.core.naturalize.time.naturaldelta(
                    self.core.chronologize.now() - discord.utils.snowflake_time(self.bot.user.id)
                ),
            )
        )
        await ctx.send(embed=embed, ephemeral=True)

    @commands.hybrid_command()
    @commands.guild_only()
    @commands.bot_has_permissions(embed_links=True)
    async def serverinfo(self, ctx: commands.Context, details: bool = False):
        """
        Show server information.

        `details`: Shows more information when set to `True`.
        Default to False.
        """
        created_at = "Created on {date}. That's about {num} days ago!".format(
            date=ctx.guild.created_at.strftime("%d %b %Y %H:%M"),
            num=self.core.naturalize.number.intword(
                (ctx.message.created_at - ctx.guild.created_at).days
            ),
        )
        online = self.core.naturalize.number.intword(
            len([m.status for m in ctx.guild.members if m.status != discord.Status.offline])
        )
        total_users = self.core.naturalize.number.intword(ctx.guild.member_count)
        text_channels = self.core.naturalize.number.intword(len(ctx.guild.text_channels))
        voice_channels = self.core.naturalize.number.intword(len(ctx.guild.voice_channels))
        if not details:
            data = discord.Embed(description=created_at, color=discord.Color.random())
            data.add_field(name="Users online", value=f"{online}/{total_users}")
            data.add_field(name="Text Channels", value=text_channels)
            data.add_field(name="Voice Channels", value=voice_channels)
            data.add_field(
                name="Roles", value=self.core.naturalize.number.intword(len(ctx.guild.roles))
            )
            data.add_field(name="Owner", value=ctx.guild.owner.display_name)
            data.set_footer(text="Server ID: " + str(ctx.guild.id))
            data.set_author(name=ctx.guild.name)
            if ctx.guild.icon.url:
                data.set_thumbnail(url=ctx.guild.icon.url)
        else:

            shard_info = (
                "\nShard ID: **{shard_id}/{shard_count}**".format(
                    shard_id=str(ctx.guild.shard_id + 1),
                    shard_count=str(ctx.bot.shard_count),
                )
                if ctx.bot.shard_count > 1
                else ""
            )
            # Logic from: https://github.com/TrustyJAID/Trusty-cogs/blob/master/serverstats/serverstats.py#L159
            online_stats = {
                "Humans: ": lambda x: not x.bot,
                " • Bots: ": lambda x: x.bot,
                "\N{LARGE GREEN CIRCLE}": lambda x: x.status is discord.Status.online,
                "\N{LARGE ORANGE CIRCLE}": lambda x: x.status is discord.Status.idle,
                "\N{LARGE RED CIRCLE}": lambda x: x.status is discord.Status.do_not_disturb,
                "\N{MEDIUM WHITE CIRCLE}": lambda x: x.status is discord.Status.offline,
                "\N{LARGE PURPLE CIRCLE}": lambda x: any(
                    a.type is discord.ActivityType.streaming for a in x.activities
                ),
                "\N{MOBILE PHONE}": lambda x: x.is_on_mobile(),
            }
            member_msg = "Users online: **{online}/{total_users}**\n".format(
                online=online, total_users=total_users
            )
            count = 1
            for emoji, value in online_stats.items():
                try:
                    num = len([m for m in ctx.guild.members if value(m)])
                except Exception as error:
                    print(error)
                    continue
                else:
                    member_msg += (
                        f"{emoji} {'**'+self.core.naturalize.number.intword(num)+'**'} "
                        + ("\n" if count % 2 == 0 else "")
                    )
                count += 1

            verif = {
                "none": "0 - None",
                "low": "1 - Low",
                "medium": "2 - Medium",
                "high": "3 - High",
                "extreme": "4 - Extreme",
            }

            guild_features_list = [
                f"✅ {feature.replace('_', ' ').title()}" for feature in ctx.guild.features
            ]

            joined_on = "{bot_name} joined this server on {bot_join}. That's {since_join} days ago!".format(
                bot_name=ctx.bot.user.display_name,
                bot_join=ctx.guild.me.joined_at.strftime("%d %b %Y %H:%M:%S"),
                since_join=self.core.naturalize.number.intword(
                    (ctx.message.created_at - ctx.guild.me.joined_at).days
                ),
            )

            data = discord.Embed(
                description=(f"{ctx.guild.description}\n\n" if ctx.guild.description else "")
                + created_at,
                color=discord.Color.random(),
            )
            if "VERIFIED" in ctx.guild.features:
                data.set_author(
                    name=ctx.guild.name,
                    icon_url="https://cdn.discordapp.com/emojis/457879292152381443.png",
                )
            elif "PARTNERED" in ctx.guild.features:
                data.set_author(
                    name=ctx.guild.name,
                    icon_url="https://cdn.discordapp.com/emojis/508929941610430464.png",
                )
            else:
                data.set_author(
                    name=ctx.guild.name,
                )
            if ctx.guild.icon.url:
                data.set_thumbnail(url=ctx.guild.icon.url)
            data.add_field(name="Members:", value=member_msg)
            data.add_field(
                name="Channels:",
                value="\N{SPEECH BALLOON} Text: {text}\n\N{SPEAKER WITH THREE SOUND WAVES} Voice: {voice}".format(
                    text="**" + text_channels + "**", voice="**" + voice_channels + "**"
                ),
            )
            data.add_field(
                name="Utility:",
                value="Owner: {owner}\nVerif. level: {verif}\nServer ID: {id}{shard_info}".format(
                    owner="**" + ctx.guild.owner.display_name + "**",
                    verif="**" + verif[str(ctx.guild.verification_level)] + "**",
                    id="**" + str(ctx.guild.id) + "**",
                    shard_info=shard_info,
                ),
                inline=False,
            )
            data.add_field(
                name="Misc:",
                value="AFK channel: {afk_chan}\nAFK timeout: {afk_timeout}\nCustom emojis: {emoji_count}\nRoles: {role_count}".format(
                    afk_chan=(
                        "**" + ctx.guild.afk_channel.name + "**"
                        if ctx.guild.afk_channel
                        else "**Not set**"
                    ),
                    afk_timeout="**"
                    + self.core.naturalize.time.naturaldelta(value=float(ctx.guild.afk_timeout))
                    + "**",
                    emoji_count="**"
                    + self.core.naturalize.number.intword(len(ctx.guild.emojis))
                    + "**",
                    role_count="**"
                    + self.core.naturalize.number.intword(len(ctx.guild.roles))
                    + "**",
                ),
                inline=False,
            )
            if guild_features_list:
                data.add_field(name="Server features:", value="\n".join(guild_features_list))
            if ctx.guild.premium_tier != 0:
                nitro_boost = "Tier {boostlevel} with {nitroboosters} boosters\nFile size limit: {filelimit}\nEmoji limit: {emojis_limit}\nVCs max bitrate: {bitrate}".format(
                    boostlevel="**" + str(ctx.guild.premium_tier) + "**",
                    nitroboosters="**"
                    + self.core.naturalize.number.intword(ctx.guild.premium_subscription_count)
                    + "**",
                    filelimit="**"
                    + self.core.naturalize.filesize.naturalsize(ctx.guild.filesize_limit)
                    + "**",
                    emojis_limit="**" + str(ctx.guild.emoji_limit) + "**",
                    bitrate="**"
                    + self.core.naturalize.filesize.naturalsize(ctx.guild.bitrate_limit)
                    + "**",
                )
                data.add_field(name="Nitro Boost:", value=nitro_boost)
            if ctx.guild.splash:
                data.set_image(url=ctx.guild.splash.url)
            data.set_footer(text=joined_on)

        await ctx.send(embed=data, ephemeral=True)
