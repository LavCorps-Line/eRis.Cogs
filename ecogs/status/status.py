#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/libCore
## a component of eRis
* Provides utilty library for other cogs
* Exposes naturalize.py
* Exposes chronologize.py
* Exposes discord.utils
* provides ping command
* provides restart command
* provides shutdown command
* provides invite command
* provides help command
"""

# Third-Party Imports #
import discord
import discord.ext.commands as commands

# First-Party Imports #
import eris.bot
import eris.cog
from eris.logger import logger


class status(eris.cog.Cog):
    """eRis Status Cog

    * Shows custom status with useful info about the bot
    * Shows useful information about the internals of the bot"""

    def __init__(self, **kwargs) -> None:
        self.bot: eris.bot.Bot = kwargs["bot"]
        self.log = logger("eRis.cogs.status")

    @commands.hybrid_group()
    @commands.is_owner()
    async def status(self, ctx: commands.Context):
        """eRis Status Viewing"""
        pass

    @status.command()
    @commands.bot_has_permissions(embed_links=True)
    async def scheduler(self, ctx: commands.Context):
        """View scheduler stats"""
        async with ctx.typing(ephemeral=True):
            embed = discord.Embed(color=discord.Color.random(), title="Scheduler Status")
            embed.add_field(name="Running?", value="Yes" if self.bot.scheduler.operating else "No")
            embed.add_field(
                name="Schedule Queue Length", value=str(self.bot.scheduler.schedule.qsize())
            )
            embed.add_field(
                name="Running Tasks", value=str(len(self.bot.scheduler.running_scheduled_tasks))
            )
            await ctx.send(embed=embed, ephemeral=True)
