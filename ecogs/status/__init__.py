#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# /cogs/status
## a component of eRis.Cogs
* Shows custom status with useful info about the bot
* Shows useful information about the internals of the bot
"""

# First-Party Imports #
from eris.cog import CogImportData

# Local Imports #
from .status import status


def data(**kwargs) -> CogImportData:
    """Cog Import Data Shim"""
    return CogImportData(cog=status(**kwargs))
