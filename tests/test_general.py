#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/test_loader.py
## a component of eRis
* handles testing of eris/cogs/loader
"""

# Standard Library Imports #
import asyncio
import os
import pathlib

# Third-Party Imports #
import discord
import pytest

# First-Party Imports #
import eris.bot
import eris.exceptions as exceptions
from eris.cogs.loader import loader as _loader_cog


@pytest.mark.asyncio
async def test_general_cog_lifecycle(bot: eris.bot.Bot):
    while "loader" not in bot.coreCogs:  # Bad idea :/
        await asyncio.sleep(0)
    loader: _loader_cog = bot.coreCogs["loader"]
    await loader._loader__load_cog(
        cogName="general",
        cogPath=pathlib.Path(os.environ["CI_PROJECT_DIR"] + "/ecogs/data.toml"),
        _retry=False,
    )
    await loader._loader__unload_cog(cogName="general")
