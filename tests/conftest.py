#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# tests/conftest.py
## a component of eRis
* provides fixtures used by all tests
"""

# Standard Library Imports #
import asyncio
import os
import signal

# Third-Party Imports #
import pytest_asyncio

# First-Party Imports #
import eris
import eris.bot
import eris.logger
from eris.handler import handler as _handler
from eris.logger import logger
from eris.supervisor.__main__ import eRisEnvironment

# Defining bot fixture


@pytest_asyncio.fixture
async def bot() -> eris.bot.Bot:
    """Yields a fully functional bot"""
    environment = eRisEnvironment(
        ERIS_DISCORD_TOKEN=os.environ["DISCORD_BOT_TEST_TOKEN"],
        ERIS_CONFIG_TYPE="SQLITE",
        ERIS_SQLITE_DBPATH="/tmp/test.db",
    )
    for var in environment.env:
        os.environ[var] = environment.env[var]
    eris.__main__.environmentVerify()

    log = logger("eRis.main")  # Initialize and keep a reference to our eRis.main logger
    discordLog = logger("discord")  # Initialize and keep a reference to discord.py's logger
    discordLog.setLevel("WARNING")
    handler = _handler()  # Initialize handler
    loop = asyncio.get_running_loop()
    bot = eris.__main__.botInit(loop=loop, handler=handler)

    eris.__main__.signalRegister(loop=loop, handler=handler, bot=bot, log=log)
    await eris.__main__.eRisAuth(bot, log)
    bot.connTask = loop.create_task(bot.connect())
    await eris.__main__.eRisInit(bot, log)

    yield bot
    await asyncio.sleep(5)
    if handler.shutdownTask:
        await handler.shutdownTask
    else:
        await handler.shutdownHandler(
            bot, cause=handler.shutdownCauses.caught_signal, signal=signal.SIGINT
        )


@pytest_asyncio.fixture
async def bot_psql() -> eris.bot.Bot:
    """Yields a fully functional bot"""
    environment = eRisEnvironment(
        ERIS_DISCORD_TOKEN=os.environ["DISCORD_BOT_TEST_TOKEN"],
        ERIS_CONFIG_TYPE="PSQL",
        ERIS_PSQL_DBNAME=os.environ["POSTGRES_DB"],
        ERIS_PSQL_USERNAME=os.environ["POSTGRES_USER"],
        ERIS_PSQL_PASSWORD=os.environ["POSTGRES_PASSWORD"],
        ERIS_PSQL_HOST="postgres",
    )
    for var in environment.env:
        os.environ[var] = environment.env[var]
    eris.__main__.environmentVerify()

    log = logger("eRis.main")  # Initialize and keep a reference to our eRis.main logger
    discordLog = logger("discord")  # Initialize and keep a reference to discord.py's logger
    discordLog.setLevel("WARNING")
    handler = _handler()  # Initialize handler
    loop = asyncio.get_running_loop()
    bot = eris.__main__.botInit(loop=loop, handler=handler)

    eris.__main__.signalRegister(loop=loop, handler=handler, bot=bot, log=log)
    await eris.__main__.eRisAuth(bot, log)
    bot.connTask = loop.create_task(bot.connect())
    await eris.__main__.eRisInit(bot, log)

    yield bot
    await asyncio.sleep(5)
    if handler.shutdownTask:
        await handler.shutdownTask
    else:
        await handler.shutdownHandler(
            bot, cause=handler.shutdownCauses.caught_signal, signal=signal.SIGINT
        )
